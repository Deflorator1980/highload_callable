package book;

import java.util.concurrent.*;

/**
 * Created by isakow on 15.03.2017.
 */
public class HuTask {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        MyCallable callable1 = new MyCallable(1000);
        MyCallable callable2 = new MyCallable(2000);
        FutureTask<String> futureTask1 = new FutureTask<>(callable1);
        FutureTask<String> futureTask2 = new FutureTask<>(callable2);
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.execute(futureTask1);
        executor.execute(futureTask2);
        while (true) {
            if (futureTask1.isDone() && futureTask2.isDone()) {
                System.out.println("Done");
                executor.shutdown();
                return;
            }
            if (!futureTask1.isDone()) {
                System.out.println("futureTast1: " + futureTask1.get());
            }
            System.out.println("Waiting for futureTask2");
            String s = futureTask2.get(2000L, TimeUnit.MILLISECONDS);
            if (s != null) {
                System.out.println("futureTask2: " + s);
            }
        }
    }
}

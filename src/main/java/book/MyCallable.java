package book;

import java.util.concurrent.Callable;

/**
 * Created by isakow on 15.03.2017.
 */
public class MyCallable implements Callable{

    private long waitTime;

    public MyCallable(int timeInMillis) {
        this.waitTime = timeInMillis;
    }

    @Override
    public Object call() throws Exception {
        Thread.sleep(waitTime);
        return Thread.currentThread().getName();
    }
}

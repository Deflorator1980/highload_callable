package book;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by isakow on 15.03.2017.
 */
public class Hu implements Callable<String> {
    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return Thread.currentThread().getName();
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(4);
//        ExecutorService executor = Executors.newCachedThreadPool();
        List<Future<String>> list = new ArrayList<>();
        Callable<String> callable = new Hu();
        for (int i = 0; i < 10; i++) {
            Future<String> future = executor.submit(callable);
            list.add(future);
        }
        list.forEach(f -> {
            try {
//                System.out.println(new Date() + "::" + f.get());
                System.out.println(new Date() + "::" + f.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        executor.shutdown();
    }
}

package h;

import org.javatuples.Pair;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * Created by isakow on 15.03.2017.
 */
public class Dt {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(2);
        List<Future<String>> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(executor.submit(new KalName(i + "hui")));
        }

        executor.shutdown();

        List<Pair<LocalDateTime, Future<String>>> pairs = new ArrayList<>();
        for (Future<String> future : list) {
            pairs.add(new Pair<>(LocalDateTime.of(2017, Month.MARCH, 16, 10, new Random().nextInt(60)), future));
        }
        for (Pair<LocalDateTime, Future<String>> pair : pairs) {
            Thread.sleep(1000);
//            System.out.println(pair.getValue0() + " || " + pair.getValue1().get() + " || " + LocalDateTime.now());
            System.out.println(pair.getValue1().get() + " " + LocalDateTime.now());
        }

    }
}

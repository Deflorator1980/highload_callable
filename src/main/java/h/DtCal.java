package h;

import org.javatuples.Pair;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * Created by isakow on 17.03.2017.
 */
public class DtCal {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Pair<LocalTime, Callable>> pairs = new ArrayList<>();
//        List<Pair<LocalTime, FutureTask>> pairs = new ArrayList<>();

//        List<Future<String>> list = new ArrayList<>();
        List<Object> list = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(2);
        IntStream.iterate(10, i -> i + 5).limit(10).forEach(hui -> {
//            pairs.add(new Pair<>(LocalTime.of(12, 52, hui), new KalName("kui" + hui)));
//            pairs.add(new Pair<LocalTime, FutureTask>(LocalTime.of(12, 52, hui), new FutureTask<String>(new KalName("kui" + hui))));
            pairs.add(new Pair<>(LocalTime.of(12, 52, hui), new KalName("kui" + hui)));
        });
//        pairs.forEach(System.out::println);
        System.out.println(pairs);
        LocalTime current = LocalTime.of(12, 52, 35);

//        List<LocalTime> times = Arrays.asList(LocalTime.of(12, 52, 40));
        List<LocalTime> times = new ArrayList<>();
        times.add(LocalTime.of(12, 52, 40));
//        Thread.sleep(5000);
//        times.add(LocalTime.of(12, 52, 50));
        times.forEach(t -> {
//            pairs.stream().filter(p -> !((LocalTime) p.getValue0()).isBefore(current)).map(p -> p.getValue1()).map(executor::submit).forEach(list::add);
//            pairs.stream().filter(p -> ((LocalTime) p.getValue0()).isBefore(t)).map(p -> p.getValue1()).map(executor::submit).forEach(list::add);
            pairs.stream().filter(pt -> (pt.getValue0()).isBefore(t)).map(pc -> pc.getValue1()).map(executor::submit).forEach(list::add);
        });
        System.out.println(list);
//        times.forEach(t -> {
//            pairs.stream().filter(p -> ((Future<String>)p.getValue1()).isDone()).forEach(list::remove);
//        });
//        System.out.println(list);
        executor.shutdown();
//        Thread.sleep(4000);
//
//        List<Object> done = new ArrayList<>();
        for (Object future : list) {
            System.out.println(((Future<String>) future).get() + " " + LocalTime.now());
//            if (((Future<String>) future).isDone()) {
//                done.add(future);
//            }
        }

//        System.out.println(list);
//        done.forEach(list::remove);
//        System.out.println(list);

    }
}

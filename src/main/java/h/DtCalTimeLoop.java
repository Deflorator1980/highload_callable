package h;

import org.javatuples.Pair;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by isakow on 17.03.2017.
 */
public class DtCalTimeLoop {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Pair<LocalTime, Callable>> pairs = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(2);
        IntStream.iterate(10, i -> i + 5).limit(10).forEach(hui -> {
            pairs.add(new Pair<>(LocalTime.of(12, 52, hui), new KalName("kui" + hui)));
        });

        System.out.println(pairs);
        List<Object> list = new ArrayList<>();
        Map<Integer, Object> mapa = new HashMap<>();
        List<LocalTime> times = new ArrayList<>();
//        times.add(LocalTime.of(12, 52, 40));
        times.add(LocalTime.of(12, 52, 35));
//        times.add(LocalTime.of(12, 52, 45));
        while (true) {
            for (LocalTime t : times) {//                pairs.stream().filter(pt -> !pt.getValue0().isAfter(t)).map(pc -> pc.getValue1()).map(executor::submit).forEach(list::add);
                AtomicInteger ai = new AtomicInteger(-1);
                pairs.stream().map(p -> {
                    ai.incrementAndGet();
                    return p;
                }).filter(pt -> !pt.getValue0().isAfter(t)).map(pc -> pc.getValue1()).map(executor::submit).forEach(f -> mapa.put(ai.get(), f));

//                for (int i = 0; i < pairs.size(); i++) {
//                    Pair<LocalTime, Callable> pair = pairs.get(i);
//                    if (!pair.getValue0().isAfter(t)) {
//                        map.put(i, executor.submit(pair.getValue1()));
//                    }
//                }}
            }
            Thread.sleep(1000);
            List<Integer> removeNum = new ArrayList<>();
            mapa.entrySet().stream().filter(m -> ((Future) m.getValue()).isDone()).forEach(m -> {
                removeNum.add(m.getKey());
            });

            List<Pair<LocalTime, Callable>> killPairs = new ArrayList<>();
            removeNum.forEach(r -> killPairs.add(pairs.get(r)));
            pairs.removeAll(killPairs);

            list.addAll(mapa.values());
            for (Object future : list) {
                System.out.println(((Future) future).isDone() + " " + ((Future) future).get() + " " +   future + " " + LocalTime.now());
            }
            break;
        }
        executor.shutdown();
//        System.out.println(pairs.remove(0));
        System.out.println(pairs);
    }
}

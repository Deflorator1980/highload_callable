package e;

import h.KalName;
import org.javatuples.Pair;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by isakow on 15.03.2017.
 */
public class Exp {
    public static void main(String[] args) throws InterruptedException {

//        List<Pair<String, String>> list = new ArrayList<>();
//        list.add(new Pair<>("one", "ONE"));
//        list.add(new Pair<>("two", "TWO"));
//        System.out.println(list);

//        AtomicInteger ai = new AtomicInteger();
//        ai.incrementAndGet();
//        System.out.println(ai);
//        Integer i = ai.get();

        Map<String, String> mapa = new HashMap<>();
        mapa.put("one", "ONE");
        mapa.put("two", "TWO");

        mapa.entrySet().stream().forEach(m -> System.out.println(m.getKey() + m.getValue()));
    }
}
